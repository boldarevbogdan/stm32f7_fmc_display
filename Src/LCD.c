/*
 * LCD.c
 *
 *  Created on: 19 нояб. 2017 г.
 *      Author: bogdan
 */

#include "LCD.h"

void LCDWriteCmd (uint16_t cmd)
{
	*(uint16_t *)LCD_CMD = cmd;
}

void LCDWriteData (uint16_t data)
{
	*(uint16_t *)LCD_DATA = data;
}

void LCDWriteReg (uint16_t reg, uint16_t data);

void LCDInit (void)
{
#ifdef ILI9341
	LCDWriteCmd (0x01);	// software reset
	HAL_Delay (5);
	LCDWriteCmd (0x28);	// выключаем дисплей
	HAL_Delay (10);

	LCDWriteCmd (0x36);
	LCDWriteData (0xE8);

	LCDWriteCmd (0xF2);    // 3Gamma Function Disable
	LCDWriteData (0x00);

	LCDWriteCmd (0x26);    //Gamma curve selected
	LCDWriteData (0x01);

	LCDWriteCmd (0xE0);    //Set Gamma
	LCDWriteData (0x0F);
	LCDWriteData (0x31);
	LCDWriteData (0x2B);
	LCDWriteData (0x0C);
	LCDWriteData (0x0E);
	LCDWriteData (0x08);
	LCDWriteData (0x4E);
	LCDWriteData (0xF1);
	LCDWriteData (0x37);
	LCDWriteData (0x07);
	LCDWriteData (0x10);
	LCDWriteData (0x03);
	LCDWriteData (0x0E);
	LCDWriteData (0x09);
	LCDWriteData (0x00);

	LCDWriteCmd (0xE1);    //Set Gamma
	LCDWriteData (0x00);
	LCDWriteData (0x0E);
	LCDWriteData (0x14);
	LCDWriteData (0x03);
	LCDWriteData (0x11);
	LCDWriteData (0x07);
	LCDWriteData (0x31);
	LCDWriteData (0xC1);
	LCDWriteData (0x48);
	LCDWriteData (0x08);
	LCDWriteData (0x0F);
	LCDWriteData (0x0C);
	LCDWriteData (0x31);
	LCDWriteData (0x36);
	LCDWriteData (0x0F);

	LCDWriteCmd (0x3A);
	LCDWriteData (0x05);  	// MCU interface 16 bit per pixel
	LCDWriteCmd (0x00);	// NOP

	LCDWriteCmd (0x11);	// выходим из спящего режима
	HAL_Delay (120);

	LCDWriteCmd (0x29);	// включаем дисплей

	HAL_Delay (1);
#endif

#ifdef ILI9325
	LCDWriteReg (0x0000, 0x0001);
    HAL_Delay (50);

    LCDWriteReg (0x00E5, 0x78F0);
    LCDWriteReg (0x0001, 0x0100);
    LCDWriteReg (0x0002, 0x0700);
    LCDWriteReg (0x0003, 0x1030);
    LCDWriteReg (0x0004, 0x0000);
    LCDWriteReg (0x0008, 0x0202);
    LCDWriteReg (0x0009, 0x0000);
    LCDWriteReg (0x000A, 0x0000);
    LCDWriteReg (0x000C, 0x0000);
    LCDWriteReg (0x000D, 0x0000);
    LCDWriteReg (0x000F, 0x0000);

    LCDWriteReg (0x0010, 0x0000);
    LCDWriteReg (0x0011, 0x0007);
    LCDWriteReg (0x0012, 0x0000);
    LCDWriteReg (0x0013, 0x0000);
    LCDWriteReg (0x0007, 0x0001);
    HAL_Delay (50);

    LCDWriteReg (0x0010, 0x1090); //0x1690
    LCDWriteReg (0x0011, 0x0227);
    HAL_Delay (50);

    LCDWriteReg (0x0012, 0x001F); //0x009D
    HAL_Delay (50);

    LCDWriteReg (0x0013, 0x1500); //0x1900
    LCDWriteReg (0x0029, 0x0027); //0x0025
    LCDWriteReg (0x002B, 0x000D);
    HAL_Delay (50);

    LCDWriteReg (0x0020, 0x0000);
    LCDWriteReg (0x0021, 0x0000);
    HAL_Delay (50);

    /* Gamma adjustment from CubeF3 */
    LCDWriteReg (0x0030, 0x0007);
    LCDWriteReg (0x0031, 0x0302);
    LCDWriteReg (0x0032, 0x0105);
    LCDWriteReg (0x0035, 0x0206);
    LCDWriteReg (0x0036, 0x0808);
    LCDWriteReg (0x0037, 0x0206);
    LCDWriteReg (0x0038, 0x0504);
    LCDWriteReg (0x0039, 0x0007);
    LCDWriteReg (0x003C, 0x0105);
    LCDWriteReg (0x003D, 0x0808);

    HAL_Delay (50);

    LCDWriteReg (0x0050, 0x0000);
    LCDWriteReg (0x0051, 0x00EF);
    LCDWriteReg (0x0052, 0x0000);
    LCDWriteReg (0x0053, 0x013F);
    LCDWriteReg (0x0060, 0xA700);
    LCDWriteReg (0x0061, 0x0001);
    LCDWriteReg (0x006A, 0x0000);

    LCDWriteReg (0x0080, 0x0000);
    LCDWriteReg (0x0081, 0x0000);
    LCDWriteReg (0x0082, 0x0000);
    LCDWriteReg (0x0083, 0x0000);
    LCDWriteReg (0x0084, 0x0000);
    LCDWriteReg (0x0085, 0x0000);

    LCDWriteReg (0x0090, 0x0010);
    LCDWriteReg (0x0092, 0x0600);
    LCDWriteReg (0x0007, 0x0133);

    HAL_Delay (100);
#endif
}

void LCDFill (uint16_t color)
{
#ifdef ILI9341
	LCDWriteCmd(0x2A);
	LCDWriteData(0x0000);
	LCDWriteData(0x0000);
	LCDWriteData((LCD_X_SIZE - 1) >> 8);
	LCDWriteData((LCD_X_SIZE - 1));

	LCDWriteCmd(0x2B);
	LCDWriteData(0x0000);
	LCDWriteData(0x0000);
	LCDWriteData((LCD_Y_SIZE - 1) >> 8);
	LCDWriteData((LCD_Y_SIZE - 1));

	LCDWriteCmd(0x2C);	// eiiaiaa ia caienu a iaiyou

	for (uint32_t i = 0; i < 76800; i++)
	{
		LCDWriteData(color);
	}

	LCDWriteCmd(0x00);	// NOP
#endif

#ifdef ILI9325
	LCDWriteReg (0x0020, 0);
    LCDWriteReg (0x0021, 0);		/* LCD_Set_Cursor (0, 0); */
    LCDWriteCmd (0x0022); 			/* LCD_Write_RAM_Prepare(); */

    for (uint32_t i = 0; i < (LCD_Y_SIZE * LCD_X_SIZE); i++)
    {
    	LCDWriteData (color);
    }
#endif
}

void LCDWriteReg (uint16_t reg, uint16_t data)
{
	LCDWriteCmd (reg);
	LCDWriteData (data);
}
