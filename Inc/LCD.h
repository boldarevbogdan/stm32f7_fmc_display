/*
 * LCD.h
 *
 *  Created on: 19 нояб. 2017 г.
 *      Author: bogdan
 */

#ifndef LCD_H_
#define LCD_H_

#include <stdint.h>
#include "stm32f7xx_hal.h"

#define LCD_Y_SIZE 	240
#define LCD_X_SIZE 	320

#define LCD_DATA 	0x60100000
#define LCD_CMD 	0x60000000

// #define ILI9325
#define ILI9341

void LCDInit (void);
void LCDFill (uint16_t color);

#endif /* LCD_H_ */
